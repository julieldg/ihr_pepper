
function goToCocktailPage(cocktailName) {
    // URL du fichier de recette sur GitHub
    const recipeUrl = `https://raw.githubusercontent.com/Zukero/Cocktails-recipes/main/cocktails/${cocktailName}.md`;

    fetch(recipeUrl)
        .then(response => response.text())
        .then(data => {
            // Afficher la recette sur la page ou la traiter comme nécessaire
            alert(data); // Cet exemple utilise simplement une alerte pour montrer la recette
        })
        .catch(error => console.error('Erreur lors de la récupération de la recette:', error));
}


function goToPoisonPage(cocktailName) {
    window.location.href = cocktailName + ".html";
}

