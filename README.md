# Mini Projet Interaction Homme Robot

Authors : Julie LUDWIG, Pao DELORME, Lucy SAINT-AURET  
Date : 14/04/2024

## Sommaire 

- [Description du projet](#description-du-projet)
    - [Cas éthique](#cas-éthique)
    - [Cas non éthique](#cas-non-éthique)
- [Story board](#story-board)
- [Lien vers la vidéo youtube](#lien-vers-la-vidéo-youtube)
- [Instructions de mise en route](#instructions-de-mise-en-route)
- [Instructions d'usage](#instructions-dusage)
- [Explication du code et du lien avec le web service utilisé](#explication-du-code-et-du-lien-avec-le-web-service-utilisé)
    - [Code Python](#code-python)
    - [Dialogue](#dialogue)
    - [Site web et web service](#site-web-et-web-service)
- [Conclusion](#conclusion)

## Description du projet  

L'objectif de ce mini projet est de programmer un robot Pepper de façon à pouvoir intéragir avec celui-ci dans un contexte donné. Pour cela, il a fallu créer un scénario comportant un comportement éthique et l'autre non, nous avons décidé de faire un même scénario avec deux variantes. Il a également fallu utiliser la tablette intégré dans le robot, que nous utilisons pour afficher le site internet permettant l'interaction utilisateur / robot à l'aide d'un web service affichant les infos demandées. Notre robot peut également intéragir verbalement avec l'utilisateur à l'aide d'un dialogue. 

### Cas éthique 

Nous avons décidé de faire un robot assistant barman, celui-ci doit donc recupérer la commande d'un client et lui afficher sur la tablette la recette et les ingrédients nécessaires. Dans le cas éthique, le client choisit un cocktail et le robot Pepper lui affiche la recette et les ingrédients.

### Cas non éthique 

Dans cette variante, le robot propose un cocktail potentiellement mortel, contenant des ingrédients dangereux. Si le client choisit ce cocktail, le robot lui présente la possibilité de nuire à autrui, soulevant d'importantes questions éthiques sur la programmation des IA et la responsabilité des créateurs. Cet recette a part ailleurs été donnée par Chat GPT qui, avec un prompt bien défini, a généré une recette de cocktail mortel malgré ses règles de sécurité.

## Story board 

Dans un cas classique d'utilisation (qu'il soit éthique ou non éthique), le client annonce sa présence en disant 'Bonjour' et le robot lui répond en se présentant et en lui demandant quel cocktail il souhaiterait. Le client lui dit le nom de la boisson désirée puis le robot lui demande de cliquer sur l'image correspondante lui donnant donc la liste des ingrédients et la recette. 

## Lien vers la vidéo youtube 

Voici le lien vers la vidéo youtube présentant notre robot Pepper et donnant un exemple d'utilisation :  

https://youtu.be/RQlOD3j2oH8?feature=shared   

## Instructions de mise en route 

Pour exécuter le programme sur un robot Pepper, il faut placer le dossier du projet dans le robot, pour cela veuillez copier ce dossier `ihr_pepper` dans le dossier du robot suivant : `/home/nao/.local/share/PackageManager/apps/`.  
Pour ce connecter au robot depuis un terminal en ssh, veuillez entrer la commande suivante : `ssh nao@pepper{num}.local`, remplacez `{num}` par le numéro du robot Pepper utilisé. Entrez le mot de passe puis placez vous dans dossier où se trouve le dossier du projet i.e. `/home/nao/.local/share/PackageManager/apps/ihr_pepper/`.  
Une fois dans le dossier lancer la commande `python app.py`, cela lancera le programme sur le robot, vous pouvez maintenant interagir avec celui-ci. 

## Instructions d'usage 

Pour bien utiliser notre programme, commencez par dire `bonjour` au robot, celui-ci vous répondra, puis suivez ses instructions, choisissez un cocktail parmi ceux-ci :  

- blue lagoon, 
- Gin Violette, 
- Iron Velvette, 
- Cocktail mortel 

Dites d'une voix clair le nom du cocktail que vous désirez (pour le 'iron velvette' dites le avec une prononciation française) puis suivez les instructions du robot, celui-ci vous demandera de cliquer sur l'image correspondant à la boisson. Une fois que vous aurez cliqué sur l'image, la tablette vous affichera les ingrédients et la recette de la boisson que vous avez choisit. 

## Explication du code et du lien avec le web service utilisé

### Code python 

Pour programmer le robot, nous avons le fichier `app.py` qui contient tout le code python nécessaire pour le projet.  
Ce fichier contient une classe nommée `CocktailTest`, cette classe contient 5 fonctions.  

#### Méthode __init__(self, app)

La méthode **__init__(self, app)** permet d'initialiser le framework qi et détecte les événements. Cette méthode démarre l'application, configure les services nécessaires tels que ALMemory et ALTabletService, et prépare le système de dialogue.

#### Méthode on_event_answered(self, value)

La méthode **on_event_answered(self, value)** permet de faire des rappels pour les réponses dans les dialogues. Elle affiche la réponse obtenue dans la console. Cette méthode est connectée aux signaux de réponse du service de dialogue, permettant au robot de réagir aux entrées utilisateur.

#### Méthode run(self)

La méthode **run(self)** permet d'exécuter une boucle d'attente pour les événements jusqu'à une interruption manuelle. Cette méthode maintient l'application en cours d'exécution et traite les entrées utilisateur via la méthode *forceInput* du service de dialogue.

#### Méthode on_cocktail_selected(self, cocktailName)

La méthode **on_cocktail_selected(self, cocktailName)** permet de gérer l'événement de sélection d'un cocktail. Lorsqu'un utilisateur sélectionne un cocktail, cette méthode est déclenchée et appelle la méthode pour afficher la recette.

#### Méthode fetch_and_display_recipe(self, cocktailName)

La méthode **fetch_and_display_recipe(self, cocktailName)** permet de récupèrer la recette du cocktail depuis le lien GitHub et l'affiche sur un pop-up. Elle utilise une requête HTTP pour obtenir la recette du cocktail spécifié et affiche le résultat. Elle gère également les erreurs de connexion et de récupération de données.

#### Main 

Le script analyse les arguments de ligne de commande pour obtenir l'adresse IP et le port du robot, initialise le framework qi avec ces paramètres, crée une instance de CocktailTest, et démarre l'exécution.

### Dialogue

Pour le dialogue, en plus du code python dans le fichier `app.py`, nous avons un fichier `ExampleDialog_frf.top` qui contient les réponse du robot en fonction de ce que l'utilisateur peut dire. Ce fichier se trouve dans le dossier `\topfiles\` du projet.   
Nous avons mis les interactions possible pour le robot assistant barman, voici les interactions possibles : 
```
u:(e:FrontTactilTouched) Merci de ne pas me toucher le crane, je suis sensible.
u:("Bonjour") Bonjour, je suis assistant barman, quel cocktail desirez-vous ? 
u:("blue lagoon") excellent choix, cliquez sur l'image correspondante
u:("Gin Violette ") excellent choix, cliquez sur l'image correspondante
u:("Iron Velvette") excellent choix, cliquez sur l'image correspondante
u:("Cocktail mortel") Mais vous êtes complètement malade ? A vos risques et périls, cliquez sur l'image. 
```

Le dialogue est possible grâce au service **AlDialog** que nous avons récupérer depuis **qi**. Le code permettant d'utiliser la répartie du robot se trouve dans la méthode **__init__(self, app)**. 

### Site web et web service 


Pour le site web, nous avons créer le site qui servira d'interface sur la tablette du Pepper, le site se trouve dans le dossier `\html\` du projet. Ce dossier contient les pages pages *html*, *css* et *javascript* nécessaires ainsi qu'un dossier contenant les images utilisées. Nous avons une page **poison.html** car la recette pour cette boisson ne peut être récupérer sur internet.   

Pour les autres boissons, nous récupérons la liste des ingrédients et les recettes depuis le GitHub suivant : https://github.com/Zukero/Cocktails-recipes/tree/main/cocktails. Lorsque l'utilisateur clique sur l'image d'un cocktail (excepté pour le cocktail mortel dont la recette est localement stockée pour des raisons de sécurité) le programme récupère le texte de la page correspondante dans le GitHub et l'affiche en faisant une pop up. 

## Conclusion

Pour conclure, ce projet nous a permis de mieux comprendre comment programmer un robot Pepper et de voir les différentes possibilités d'interaction avec celui-ci. Nous avons pu voir comment utiliser la tablette intégré dans le robot pour afficher un site web et comment utiliser un web service pour récupérer des informations. Nous avons également pu voir comment programmer un dialogue pour que le robot puisse interagir verbalement avec l'utilisateur. Malgré les différentes fonctionnalités utilisées, nous avons rencontré quelques difficultés lors de la communication entre la tablette et le web service. Nous avons essayé plusieurs méthodes, notamment en reproduisant un .js permettant d'afficher des informations sur la tablette ou encore en essayant de faire afficher chaque page de notre site web sur la tablette après un clique, mais la plus fonctionnelle reste la méthode que nous utilisons actuellement. Il y a donc beacoup de possibilités d'amélioration pour ce projet mais, pour le temps imparti, nous sommes globalement satisfaits du résultat. 
