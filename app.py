#! /usr/bin/env python
# -*- encoding: UTF-8 -*-



import qi
import time
import sys
import argparse
import webbrowser
import requests

class CocktailTest(object):
    """
    Classe permettant d'afficher une recette cocktail
    """

    def __init__(self, app):
        """
        Initialisation de la classe CocktailTest avec l'application qi en paramètre.
        """
        super(CocktailTest, self).__init__()
        app.start()
        session = app.session
        # Get the service ALMemory.
        self.memory = session.service("ALMemory")

        self.subscriber = self.memory.subscriber("Dialog/Answered")
        self.subscriber.signal.connect(self.on_event_answered)
        # Get the service ALTablet
        try:
            tabletService = session.service("ALTabletService")
            tabletService.loadApplication("LUDWIG_DELORME_SAINT-AURET")
            tabletService.showWebview()

        except Exception, e:
            print "Error was: ", e

        # Getting the service ALDialog
        try:
            self.ALDialog = session.service("ALDialog")
            self.ALDialog.resetAll()
            self.ALDialog.setLanguage("French")
            self.topic_name = self.ALDialog.loadTopic("/home/nao/.local/share/PackageManager/apps/LUDWIG_DELORME_SAINT-AURET/topfiles/ExampleDialog_frf.top")
            self.ALDialog.activateTopic(self.topic_name)
            self.ALDialog.subscribe('ExampleDialog')

        except Exception, e:
            print "Error was: ", e

  
    def on_event_answered(self, value):
        """
        Affiche la réponse du robot.
        """
        print "R: " + value

    def run(self):
        """
        Boucle principale du programme.
        """
        print "Starting HumanGreeter"
        try:
            while True:
                fi = raw_input('H: ')
                self.ALDialog.forceInput(fi)
                self.ALDialog.forceOutput()
                time.sleep(1)
        except KeyboardInterrupt:
            print "Interrupted by user, stopping HumanGreeter"

            self.ALDialog.unsubscribe('ExampleDialog')
            self.ALDialog.deactivateTopic(self.topic_name)
            self.ALDialog.unloadTopic(self.topic_name)
            sys.exit(0)

    def on_cocktail_selected(self, cocktailName):
        """
        Cette méthode est appelée lorsque l'événement CocktailApp/CocktailSelected est déclenché.
        """
        print("Cocktail sélectionné: ", cocktailName)
        self.fetch_and_display_recipe(cocktailName)

    def fetch_and_display_recipe(self, cocktailName):
        """
        Récupère la recette du cocktail depuis GitHub et l'affiche sur la tablette.
        """
        recipeUrl = "https://raw.githubusercontent.com/Zukero/Cocktails-recipes/main/cocktails/${cocktailName}.md"
        try:
            response = requests.get(recipeUrl)
            if response.status_code == 200:
                recipeText = response.text
                # Afficher la recette sur la tablette. Vous pouvez aussi l'afficher d'une autre manière.
                # Par exemple, en utilisant le service ALTabletService pour afficher la recette dans un format plus convivial.
                print("Recette: ", recipeText)
            else:
                print("Erreur lors de la récupération de la recette.")
        except Exception as e:
            print("Erreur lors de la récupération de la recette: ", e)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="127.0.0.1",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                            help="Naoqi port number")

    args = parser.parse_args()
    try:
        # Initialize qi framework.
        connection_url = "tcp://" + args.ip + ":" + str(args.port)
        app = qi.Application(["LUDWIG_DELORME_SAINT-AURET", "--qi-url=" + connection_url])
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)

    test = CocktailTest(app)
    test.run()
